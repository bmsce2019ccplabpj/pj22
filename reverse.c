#include<stdio.h>
int input()
{
	int num;
	printf("Enter a number:\n");
	scanf("%d",&num);
	return num;
}
int compute(int num)
{
	int rem,rev=0;
	while(num>0)
		{
			rem=num%10;
			rev=rev*10+rem;
			num=num/10;
		}
	return rev;		
}
void output(int num,int rev)
{
	if(rev==num)
		{
	 		printf("The number %d is a palindrome",num);
		}
	else
		{
			printf("The number %d is not a palindrome",num);
		}
	return ;
}
int main()
{
	int n,r;
	n=input();
	r=compute(n);
	output(n,r);
	return 0;
}
	
	