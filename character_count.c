#include<stdio.h> 
#include<ctype.h>
char input()
{
	char ch;
	printf("Enter a character:\n");
	scanf(" %c",&ch);
	return ch;
}
void compute(char ch, int *l, int *u, int *n)
{
	*l=0,*u=0,*n=0;
	do
		{
			if(islower(ch))
				{
					*l=*l+1;
				}
		  if(isupper(ch))
				{
					*u=*u+1;
				}
			if(isdigit(ch))
				{
					*n=*n+1;
				}
			printf("Enter a character or * to exit:\n");
			scanf(" %c",&ch);
		}while(ch!='*');
	return;
}
void output(int l, int u, int n)
{
	printf("number of lowercase characters = %d",l);
	printf("number of uppercase characters = %d",u);
	printf("number of digits = %d",n);
}
int main()
{
	char c;
	int l,u,n;
	c=input();
	compute(c,&l,&u,&n);
	output(l,u,n);
	return 0;
}