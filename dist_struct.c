#include <stdio.h>
#include <math.h>
struct point
{
	float x;
	float y;
};
typedef struct point Point;
Point input()
{
	Point P;
	printf("enter x and y co-ordinates of the point\n");
	scanf("%f%f",&P.x,&P.y);
	return P;
}
float compute(Point P1, Point P2)
{
	float d;
	d=sqrt((P2.x-P1.x)*(P2.x-P1.x)+(P2.y-P1.y)*(P2.y-P1.y));
	return d;
}
void output(float d)
{
	printf("the distance between the points is %f",d);
}
int main()
{
	Point P1, P2;
	float d;
	P1=input();
	P2=input();
	d=compute(P1,P2);
	output(d);
	return 0;
}
	