#include<stdio.h>
int size()
{
    int n;
    printf("Enter the size of the array:\n");
    scanf("%d",&n);
	return n;
}
void get_array(int n, int a[n],int *searchele)
{
	int i;
    printf("Enter the %d elements into the array:\n",n);
    for(i=0;i<n;i++)
        {
            scanf("%d",&a[i]);
        }
    printf("Enter the searchelement:\n");
    scanf("%d",searchele);
}
void compute(int n, int a[n], int searchele,int *found, int *mid)
{
	int lower=0,upper=n-1;
    while(lower<=upper)
        {
            *mid=(lower+upper)/2;
            if(a[*mid]==searchele)
                {
                    *found=1;
					break;
                }
            else if(a[*mid]>searchele)
                {
                    lower=*mid+1;
				}
			else
				{
				upper=*mid-1;
				}
		}
	return ;
}
void output(int found,int searchele, int mid)
{
		if(found==1)
		{
			printf("The searchelement %d is present at index %d\n",searchele,mid);
		}
		else
		{
			printf("The element not found");
		}
}
int main()
{
	int n,ele,f,m;
	n=size();
	int a[n];
	get_array(n,a,&ele);
	compute(n,a,ele,&f,&m);
	output(f,ele,m);
	return 0;
}
	
