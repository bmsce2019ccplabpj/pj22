#include<stdio.h>
int input1()
{
    int n;
    printf("Enter the size of the array:\n");
    scanf("%d",&n);
	return n;
}
void input2(int n, int a[n],int *searchele)
{
	for(int i=0;i<n;i++)
    {
         printf("Enter  elements into the array:\n");
         scanf("%d",&a[i]);
    }
    printf("Enter the element to be searched:\n");
    scanf("%d",searchele);
}
void compute(int n, int a[n],int searchele, int *found, int *position )
{
	*found=0;
    for(int i=0;i<n;i++)
    {
        if(a[i]==searchele)
        {
            *found=1;
            *position=i;
			break;
        }
    }
}
void output(int searchele,int found, int position)
{
	 if(found==1)
    {
        printf("The element %d is present at the position %d",searchele,position);
    }
    else
    {
        printf("The element is not present inside in the array");
    }
    return 0;
}
   int main()
   {
	   int n,ele,f,p;
	   n=input1();
	   int a[n];
	   input2(n,a,&ele);
	   compute(n,a,ele,&f,&p);
	   output(ele,f,p);
	   return 0;
   }

    
   