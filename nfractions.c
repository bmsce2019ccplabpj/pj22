/* Sum of n fractions using structure and gcd*/
#include<stdio.h>
struct fract
{
	int num;
	int denom;
};
typedef struct fract Fract;
int get_size() 
{
	int n;
	printf("Enter the number of fractions:\n");
	scanf("%d",&n);
	return n;
}
void get_array(int n, Fract a[n])
{
	int i;
	for(i=0;i<n;i++)
	{
		printf("Enter the fraction in n/d form:\n");
		scanf("%d/%d",&a[i].num,&a[i].denom);
	}
}
Fract compute(int n, Fract a[n])
{
	int i,g;
	Fract sum;
	sum.num=a[0].num;
	sum.denom=a[0].denom;
	for(i=1;i<n;i++)
	{
		sum.num=(sum.num*a[i].denom)+(sum.denom*a[i].num);
		sum.denom=sum.denom*a[i].denom;
	}
	g=gcd(sum.num,sum.denom);
	sum.num=sum.num/g;
	sum.denom=sum.denom/g;
	return sum;
}
int gcd(int b, int c)
{
	int t,r; 
	while(c!=0)
	{
		t=c;
		r=b%c;
		c=r;
		b=t;
		
	}
	return b;
}

void output(int n, Fract a[n], Fract sum)
{
	for(int i=0;i<n;i++)
	{
		if(i==(n-1))
		{
			printf("%d/%d",a[i].num,a[i].denom);
		}
		else
		{
			printf("%d/%d + ",a[i].num,a[i].denom);
		}
		
	}
	printf(" = %d/%d",sum.num,sum.denom);
}
int main()
{
	int n;
	n=get_size();
	Fract a[n],s;
	get_array(n,a);
	s=compute(n,a);
	output(n,a,s);
	return 0;
	
}