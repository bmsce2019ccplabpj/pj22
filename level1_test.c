#include<stdio.h>
#include<string.h>
#include<stdlib.h>
int no_instance()
{
	int s;
	printf("Enter the number of instances of the program:\n");
	scanf("%d",&s);
	return s;
}
void input(char s[])
{
	printf("\nEnter a string:\n");
	scanf("%s",s);
}
void output(char s[])
{
	puts(s);
}
void repeat(int s, char s1[], char s2[])
{
	int n;
	for(int i=1;i<=s;i++)
	{
		printf("\nEnter number of elements of the fibonacci string to be displayed :\n");
		scanf("%d",&n);
		input(s1);
		input(s2);
		printf("\n");
		output(s1);
		output(s2);
		compute(n,s1,s2);
	}
}
void compute(int n,char s1[], char s2[])
{
	
	for(int i=1;i<=n-2;i++)
	{
		if(i%2!=0)
		{
			strcat(s1,s2);
			output(s1);
		}
		else
		{
			strcat(s2,s1);
			output(s2);
		}
	}
}

int main()
{
	int n; 
	char *s1, *s2;
	n=no_instance();
	s1=malloc(n*sizeof(char));
	s2=malloc(n*sizeof(char));
	repeat(n,s1,s2);
	return 0;
	
}