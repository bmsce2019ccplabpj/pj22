#include<stdio.h>
int input()
{
    int num;
    printf("Enter a number:\n");
    scanf("%d",&num);
    return num;
}
int compute(int num)
{
    int i,flag=0;
    for( i=2; i<=num/2;i++)
        {
            if(num%i==0)
                {
                    flag=1;
                    break;
                }
        }
    return flag;
}
void output(int num,int flag)
{ 
    if(flag==0)
    {
        printf("%d is a prime number",num);
    }
    else
    {
        printf("%d is a composite number",num);
    }
}
int main()
{
	int n,flag;
	n=input();
	flag=compute(n);
	output(n,flag);
    return 0;
}