/* program to  determine whether  given pairs of circles intersect or not*/
#include<stdio.h>
#include<math.h>
struct circle
{
	float x,y,r;
};
struct circle_pair
{
	struct circle C1,C2;
	double l,d;
};
typedef struct circle_pair Cir_pair;

int get_no_pairs()
{
	int n;
	printf("Enter the number of pairs of circles you want to analyze:\n"); 
	scanf("%d",&n);
	return n;
}
void input(int n,Cir_pair C[])
{
	int i;
	printf("enter radius and co-ordinates of the circle:\n");
		for(i=0;i<n;i++)
		{
			scanf("%f%f%f",&C[i].C1.r,&C[i].C1.x,&C[i].C1.y);
			scanf("%f%f%f",&C[i].C2.r,&C[i].C2.x,&C[i].C2.y);
		}
}
void compute(int n,Cir_pair C[])
{
	int i;
	for(i=0;i<n;i++)
	{
		C[i].l=C[i].C1.r+C[i].C2.r;
		C[i].d=sqrt(pow(C[i].C2.x-C[i].C1.x,2)+pow(C[i].C2.y-C[i].C1.y,2));
	}
}
void output(int n, Cir_pair C[])
{
	int i;
	for(i=0;i<n;i++)
	{
		if(C[i].d<C[i].l)
		{
			printf("\n");
			printf(" The Circles intersect\n");
			printf("\n");
		}
		else 
		{
			printf("\n");
			printf("The Circles do not intersect\n");
			printf("\n");
		}
	}
}
int main()
{
	int n;
	Cir_pair C[50];
	n=get_no_pairs();
	input(n,C);
	compute(n,C);
	output(n,C);
	return 0;
}