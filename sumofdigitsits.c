#include<stdio.h>
int input()
{
	int num;
	printf("Enter a number:\n");
	scanf("%d",&num);
	return num;
}
int compute(int num)
{
	int sum=0,rem;
	while(num>0)
		{
			rem=num%10;
			sum+=rem;
			num=num/10;
		}
	return sum;
}
void output(int sum)
{
	printf("The sum of digits = %d",sum);
	return ;
}
int main()
{	
	int n,s;
	n=input();
	s=compute(n);
	output(s);
	return 0;
}
	
	