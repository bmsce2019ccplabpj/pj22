#include<stdio.h>
int input()
{
    int a;
    printf("Enter a number:\n");
    scanf("%d",&a);
    return a;
}
void output1(int x, int y)
{    
    printf("Before swapping:\n");
    printf("x=%d\ny=%d\n",x,y);
}
void swap(int *x, int *y)
{
    *x=*x+*y;
    *y=*x-*y;
    *x=*x-*y;
}
void output2(int x, int y)
{
    printf("After swapping:\n");
    printf("x=%d\ny=%d\n",x,y);
}
int main()
{
    int x,y;
    x=input();
    y=input();
    output1(x,y);
    swap(&x,&y);
    output2(x,y);
    return 0;
}
    