#include<stdio.h>
struct Date
{
    int date;
    int month;
    int year;
};
struct employee
{
    int id;
    char name[50];
    float salary;
    struct Date DOJ;
};
typedef struct employee Employee;
int input()
{
    int n;
    printf("Enter the number of employee's details you want to store:\n");
    scanf("%d",&n);
    return n;
}
void compute(int n,Employee E[n])
{
    int i;
    for(i=0;i<n;i++)
        {
            printf("Enter the employee%d ID number:\n",i+1);
            scanf("%d",&E[i].id);
            printf("Enter the employee%d name:\n",i+1);
            scanf("%s",&E[i].name);
            printf("Enter the employee%d's salary:\n",i+1);
            scanf("%f",&E[i].salary);
            printf("Enter the employee%d's date of join in dd-mm-yy format:\n",i+1);
            scanf("%d%d%d",&E[i].DOJ.date,&E[i].DOJ.month,&E[i].DOJ.year);
        }
    return ;
}
void output(int n,Employee E[n])
{
    int i;
    for(i=0;i<n;i++)
        {
			printf("\n");
            printf("Employee%d details:\n",i+1);
            printf("ID number: %d\n",E[i].id);
            printf("Name: %s\n",E[i].name);
            printf("Salary: %.4f\n",E[i].salary);
            printf("Date of join:%d-%d-%d\n",E[i].DOJ.date,E[i].DOJ.month,E[i].DOJ.year);
        }
    return ;
}
int main()
{
    int n;
    n=input();
    Employee E[n];
    compute(n,E);
    output(n,E);
    return 0;
}


  
    