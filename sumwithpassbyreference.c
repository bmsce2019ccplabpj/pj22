#include<stdio.h>
void input()
{
int *a;
printf("enter a number\n");
scanf("%d",&a);
}
void compute(int *a, int *b, int *c)
{
*c=*a+*b;
}
void output(int *a, int *b, int *c)
{
printf("%d+%d=%d",a,b,c);
}
int main()
{
int x,y,z;
input(&x);
input(&y);
compute(x,y,z);
output(x,y,z);
}

#include <stdio.h>
void input(int *a, int *b)
{
	printf("enter two numbers\n");
	scanf("%d%d",a,b);
}
void compute(int *a, int *b, int *c)
{
	*c=*a+*b;
}
void output(int *a, int *b, int *c)
{
	printf("%d+%d=%d",*a,*b,*c);
}
int main()
{
	int x,y,z;
	input(&x,&y);
	compute(&x,&y,&z);
	output(&x,&y,&z);
}
